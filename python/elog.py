#!/usr/bin/env python
##################################################
#
# Elog python interface
# Based on the c++ code by Stefan Ritt
# https://bitbucket.org/ritt/elog
#
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# March 2017
#
##################################################

import random
import socket

class elog:
    def __init__(self,host="localhost",port=80):
        self.host=host
        self.port=port
        self.logb=""
        self.data={}
        self.verbose=False
        pass
    def setVerbose(self,enable):
        self.verbose=enable
    def submit(self,logbook="",data={},attachments=None):
        self.logb = logbook
        self.data = data
        if self.verbose: print "Build request"
        boundary = "---------------------------%04X%04X%04X" % (random.random()*0xFFFF,
                                                                random.random()*0xFFFF,
                                                                random.random()*0xFFFF)
        content  = boundary
        content += "\r\nContent-Disposition: form-data; name=\"cmd\"\r\n\r\nSubmit\r\n"
        content += boundary
        content += "\r\nContent-Disposition: form-data; name=\"encoding\"\r\n\r\nplain\r\n"
        content += boundary
        for k in self.data:
            content +="\r\nContent-Disposition: form-data; name=\"%s\"\r\n\r\n%s\r\n" % (k,self.data[k])
            content += boundary
            pass

        if attachments:
            for i in xrange(len(attachments)):
                content +="Content-Disposition: form-data; name=\"attfile%i\"; filename=\"%s\"\r\n\r\n" %(i,attachments[i]) 
                content += open(attachments[i], 'r').read()
                content += boundary
                content +="\r\n"
                pass
            pass

        request  = "POST /%s/ HTTP/1.0\r\n" % self.logb
        request += "Content-Type: multipart/form-data; boundary=%s\r\n" % boundary
        request += "Host: %s:%s\r\n" % (self.host,self.port)
        request += "User-Agent: ELOG\r\n"
        request += "Content-Length: %d\r\n" % len(content)
        request += "\r\n"

        if self.verbose:
            print "Request:"
            print "#"*20
            print request+"\n"+content
            print "#"*20
            pass
        
        if self.verbose: print "Connect and send"
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((self.host,self.port))
        self.sock.send(request)
        self.sock.send(content)
        response = self.sock.recv(99999)
        self.sock.close()
        
        if self.verbose:
            print "Reply:"
            print "#"*20
            print response
            print "#"*20
            pass        
        
        if "302 Found" in response:
            if "Location:" in response:
                if "has moved" in response:
                    print "Error: elogd server has moved to another location"
                    return False
                if "fail" in response:
                    print "Error: Invalid user name or password"
                    return False
                else:
                    pos1 = response.find("Location:")
                    pos2 = response.find("\n",pos1)
                    chk = response[pos1:pos2]
                    chk = chk.split("/")[-1]
                    print "Message successfully transmitted, ID=%s" % chk
                    return True
                pass
            else:
                print "Message successfully transmitted"
                return True
            pass
        else:
            print "Error transmitting message: See below"
            print response
            return False
    
##        } else if (strstr(response, "Logbook Selection"))
##        printf("Error: No logbook specified\n");
##        else if (strstr(response, "enter password"))
##        printf("Error: Missing or invalid password\n");
##        else if (strstr(response, "form name=form1"))
##        printf("Error: Missing or invalid user name/password\n");
##        else if (strstr(response, "Error: Attribute")) {
##            if (strstr(response, "not existing")) {
##                strncpy(str, strstr(response, "Error: Attribute") + 27, sizeof(str));
##                if (strchr(str, '<'))
##                *strchr(str, '<') = 0;
##                printf("Error: Non existing attribute option \"%s\"\n", str);
##                } else {
##                        strncpy(str, strstr(response, "Error: Attribute") + 20, sizeof(str));
##                if (strchr(str, '<'))
##                *strchr(str, '<') = 0;
##                printf("Error: Missing required attribute \"%s\"\n", str);
##                }
##            } else
##                printf("Error transmitting message\n");
    
        pass
    pass

if __name__=="__main__":
    import argparse
    post = elog("ade-pixel-elog",8080)
    print post.submit("demo",{"Author":"Carlos","Subject":"Test","Type":"Other","Text":"This is a test"},["/tmp/adecmos/tt.txt"])
    
