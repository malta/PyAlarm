#!/usr/bin/env python
##################################################
#
# SMS & Email Alert python interface
# 
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# August 2017
#
##################################################

import os
import smtplib
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication

class alert:
    def __init__(self,fromUser,fromPwd):
        self.verbose=False
        self.fromUser = str(fromUser)
        self.fromPwd = str(fromPwd)
        self.smtpserver = smtplib.SMTP("smtp.cern.ch",587)
        self.smtpserver.ehlo()
        self.smtpserver.starttls()
        self.smtpserver.ehlo
        self.smtpserver.login(fromUser, fromPwd)
        pass
    def setVerbose(self,enable):
        self.verbose=enable
        pass
    def send(self,to,subject,message,image=None):
        header = 'To:' + to + '\n' + 'From: ' + self.fromUser + '\n' + 'Subject:%s'%subject+'\n'	
        msg = header+"\n"+message
        ret = self.smtpserver.sendmail(self.fromUser, to, msg)
        if self.verbose:
            print(header)
            print('done!')
            pass
        return ret
    def sendMime(self,to,subject,message,image=None):
        msg = MIMEMultipart()
        msg['Subject'] = subject
        msg['From'] = self.fromUser
        msg['To'] = to
        text = MIMEText(message)
        msg.attach(text)
        if image:
            ext = os.path.splitext(image)[1].lower()
            img_data = open(image, 'rb').read()
            if ext==".pdf":
                img = MIMEApplication(img_data, _subtype = "pdf")
                img.add_header('content-disposition', 'attachment', filename = ('utf-8', '', os.path.basename(image)))
            else:
                img = MIMEImage(img_data, name=os.path.basename(image))
                pass
            msg.attach(img)
            pass
        ret = self.smtpserver.sendmail(self.fromUser, to, msg.as_string())
        if self.verbose:
            print('done!')
            pass
        return ret
    def sendMime2(self,to,subject,message,files=None):
        msg = MIMEMultipart()
        msg['Subject'] = subject
        msg['From'] = self.fromUser
        msg['To'] = to
        text = MIMEText(message)
        msg.attach(text)
        for f in files:
            ext = os.path.splitext(f)[1].lower()
            img_data = open(f, 'rb').read()
            if ext==".png":
                img = MIMEApplication(img_data, _subtype = "png")
                img.add_header('content-disposition', 'attachment', filename = ('utf-8', '', os.path.basename(f)))
            else:
                img = MIMEImage(img_data, name=os.path.basename(f))
                pass
            msg.attach(img)
            pass
        ret = self.smtpserver.sendmail(self.fromUser, to, msg.as_string())
        if self.verbose:
            print('done!')
            pass
        return ret
    def close(self):
        self.smtpserver.close()
        pass
    
if __name__=="__main__":
    import argparse
    import getpass
    parser = argparse.ArgumentParser()
    parser.add_argument("-l","--login",help="login",default=getpass.getuser())
    parser.add_argument("-t","--to",help="recipient",default="%s@cern.ch"%getpass.getuser())
    parser.add_argument("-s","--subject",help="subject",default="test")
    parser.add_argument("-m","--message",help="message",default="test")
    parser.add_argument("-i","--image",help="image",default=None)
    args = parser.parse_args()
    
    pasw = getpass.getpass("Password for %s: "%args.login)
    
    post = alert("%s@cern.ch"%args.login,pasw)
    post.sendMime(args.to,args.subject,args.message,args.image)
    
