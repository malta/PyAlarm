#!/usr/bin/python
##################################################
#
# Interlock tool for thermal measurements
# 
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# August 2017
#
##################################################

import os
import sys

class Interlock:
    def __init__(self):
        self.channels={}
        self.length=10
        self.limitlo=-40
        self.limithi=40
        self.error=""
        pass
    def Reset(self):
        self.channels={}
        self.error=""
        pass
    def SetLimits(self,lowlimit,highlimit):
        self.limitlo=lowlimit
        self.limithi=highlimit
        pass
    def SetLength(self,length):
        self.length=length
        pass
    def AddChannel(self,channel,value):
        if not channel in self.channels:
            self.channels[channel]=[]
            pass
        self.channels[channel].append(value)
        if len(self.channels[channel])>self.length:
            self.channels[channel].pop(0)
            pass
        elif len(self.channels[channel])<self.length:
            return True
        #if self.GetMean(channel)<self.limitlo:
        #    self.error = "Channel %s is below low limit (%i)." % (channel,self.limitlo)
        #    self.error += "History: "
        #    for value in self.channels[channel]: self.error += "%.2f, " % value
        #    return False
        #elif self.GetMean(channel)>self.limithi:
        #    self.error = "Channel %s is above high limit (%i)." % (channel,self.limithi)
        #    self.error += "History: "
        #    for value in self.channels[channel]: self.error += "%.2f, " % value
        #    return False
        if self.GetMedian(channel)<self.limitlo:
            self.error = "Channel %s is below low limit (%i). " % (channel,self.limitlo)
            self.error += "History: "
            for value in self.channels[channel]: self.error += "%.2f, " % value
            self.error += "Median value: %s. " % self.GetMedian(channel)
            return False
        elif self.GetMedian(channel)>self.limithi:
            self.error = "Channel %s is above high limit (%i). " % (channel,self.limithi)
            self.error += "History: "
            for value in self.channels[channel]: self.error += "%.2f, " % value
            self.error += "Median value: %s. " % self.GetMedian(channel)
            return False        
    def GetMean(self,channel):
        return sum(self.channels[channel])/float(len(self.channels[channel]))
    def GetMedian(self,channel):
        sortedChannels = sorted(self.channels[channel])
        channelsLen = len(self.channels[channel])
        index = (channelsLen-1) // 2
        if (channelsLen % 2):
            return sortedChannels[index]
        else:
            return (sortedChannels[index] + sortedChannels[index+1])/2.0
    def GetError(self):
        return self.error
    pass
